### functions for the colonies, stochastic process project
### developer: Irina Mohorianu
### Jan - Mar 2020

library(ggplot2)
library(gplots)
library(gtools)
library(circlize)
library(ComplexHeatmap)

process.input <- function(input.file, presence.thr)
{
  inp = input.file
  my.labels = ""
  my.totals = c(0)
  my.counts = c(0,0,0,0)
  ##the order of classes is mk, ery, my, ly
  
  for(i in 1:nrow(inp))
  {
    if(i%%5 == 1)
    {
      my.label = as.character(inp[i,1])
      my.total = inp[i,3]
      my.count = c(0,0,0,0)
    }
    
    if((i%%5 == 2) || (i%%5 == 3) || (i%%5 == 4) || (i%%5 == 0))
    {
      if(inp[i,2] == "mk")  {my.count[1] = inp[i,3]}
      if(inp[i,2] == "ery") {my.count[2] = inp[i,3]}
      if(inp[i,2] == "my")  {my.count[3] = inp[i,3]}
      if(inp[i,2] == "ly")  {my.count[4] = inp[i,3]}
    }
    if(i%%5 == 0)
    {
      my.labels = c(my.labels, my.label) 
      my.totals = c(my.totals, my.total)
      my.counts = rbind(my.counts, my.count) 
    }
  }
  
  my.labels  = my.labels[2:length(my.labels)]
  my.totals  = my.totals[2:length(my.totals)]
  my.counts  = my.counts[2:nrow(my.counts),]
  
  ##assign custom labels
  my.assign  = rep(0,nrow(my.counts)*ncol(my.counts))
  my.assign = my.counts > presence.thr
  colnames(my.assign) = c("mk.lab", "ery.lab", "my.lab", "ly.lab")
  
  new.lab = rep("",nrow(my.assign))
  for(i in 1:nrow(my.assign))
  {
    new.lab[i] = paste(
      ifelse(my.assign[i,2],"Ery",""),
      ifelse(my.assign[i,4],"Ly",""),
      ifelse(my.assign[i,1],"Mk",""),
      ifelse(my.assign[i,3],"My",""),
      sep="")
  }
  new.lab = as.factor(new.lab)
  summary(new.lab)
  
  my.return = data.frame(my.labels, my.totals, my.counts, my.assign, new.lab)
  colnames(my.return)  = c("original.label", "total.cells", 
                           "mk.cnt", "ery.cnt", "my.cnt", "ly.cnt",
                           "mk.lab", "ery.lab", "my.lab", "ly.lab", "new.lab")
  row.names(my.return) = 1:nrow(my.return)
  return(my.return)
}

calculate.summary <- function(processed.input, label)
{
  called.cells    = apply(processed.input[,3:6],1,sum)
  undefined.cells = processed.input$total.cells - called.cells
  undefined.percentage = undefined.cells/processed.input$total.cells
  par(mfrow=c(1,2))
  hist(undefined.cells, main = "Histogram of undefined cells" )
  hist(undefined.percentage, main = "As percentages" ,xlim=c(-1,1))
  par(mfrow=c(1,1))
  
  ## summary per class.
  label.summary = summary(processed.input$original.label)
  print("MK cells");  print(summary(processed.input$mk.lab))
  print("ERY cells"); print(summary(processed.input$ery.lab))
  print("MY cells");  print(summary(processed.input$my.lab))
  print("LY cells");  print(summary(processed.input$ly.lab))
  
  ##use the calculated labels
  new.lab = rep("",nrow(processed.input))
  for(i in 1:nrow(processed.input))
  {
    new.lab[i] = paste(
                       ifelse(processed.input[i,]$ery.lab,"Ery",""),
                       ifelse(processed.input[i,]$ly.lab,"Ly",""),
                       ifelse(processed.input[i,]$mk.lab,"Mk",""),
                       ifelse(processed.input[i,]$my.lab,"My",""),
                       sep="")
  }
  new.lab = as.factor(new.lab)
  summary(new.lab)
  
  ##the expected distribution is RU [random uniform]
  cell.types = c("Ery","Ly","Mk","My")
  ##one cell set
  one.cell  = permutations(n = 4, r = 1, v = cell.types, repeats.allowed = F)
  one.cell  = data.frame(one.cell,rep(1/4, 4));colnames(one.cell) = c("label","per")
  
  ##two cell set
  two.cell  = permutations(n = 4, r = 2, v = cell.types, repeats.allowed = F)
  two.cell.lab = rep("",nrow(two.cell))
  for(i in 1:nrow(two.cell))
  {
    my.temp = sort(two.cell[i,])
    two.cell.lab[i] = paste(my.temp[1],my.temp[2],sep='')
  }  
  two.cell.lab = unique(two.cell.lab)
  two.cell = data.frame(two.cell.lab,rep(1/length(two.cell.lab),length(two.cell.lab)))
  colnames(two.cell) = c("label","per")
  
  ##three cell set
  three.cell  = permutations(n = 4, r = 3, v = cell.types, repeats.allowed = F)
  three.cell.lab = rep("",nrow(three.cell))
  for(i in 1:nrow(three.cell))
  {
    my.temp = sort(three.cell[i,])
    three.cell.lab[i] = paste(my.temp[1],my.temp[2], my.temp[3],sep='')
  }  
  three.cell.lab = unique(three.cell.lab)
  three.cell = data.frame(three.cell.lab,rep(1/length(three.cell.lab),length(three.cell.lab)))
  colnames(three.cell) = c("label","per")
  
  ##four cell set
  four.cell  = permutations(n = 4, r = 4, v = cell.types, repeats.allowed = F)
  four.cell.lab = rep("",nrow(four.cell))
  for(i in 1:nrow(four.cell))
  {
    my.temp = sort(four.cell[i,])
    four.cell.lab[i] = paste(my.temp[1],my.temp[2], my.temp[3],my.temp[4],sep='')
  }  
  four.cell.lab = unique(four.cell.lab)
  four.cell = data.frame(four.cell.lab,rep(1/length(four.cell.lab),length(four.cell.lab)))
  colnames(four.cell) = c("label","per")
  
  expected.options = rbind(one.cell, two.cell, three.cell, four.cell)
  expected.options[,2] = expected.options[,2] * 100
  summary.observed = summary(new.lab)
  observed.options = data.frame(labels(summary.observed), summary.observed / sum(summary.observed) * 400)
  colnames(observed.options) = c("label","per")
  rownames(observed.options) = 1:nrow(observed.options)
  
  chi.input = cbind(expected.options[,2], rep(0, nrow(expected.options)))
  for(i in 1:nrow(observed.options))
  {
    for(j in 1:nrow(expected.options))
    {
      if(observed.options[i,1] == expected.options[j,1])
      {
        chi.input[j,2] = observed.options[i,2]
      }
    }
  }
  
  rownames(chi.input) = expected.options[,1]
  chi.input = floor(chi.input+0.45)
  
  # print(chi.input)
  print(chisq.test(chi.input))
  
  fisher.output = rep(-1, nrow(chi.input))
  my.fisher = matrix(rep(0, 4), nrow=2)
  for(i in 1:nrow(chi.input))
  {
    if(chi.input[i,2]!= 0)
    {
      my.fisher[1,1] = chi.input[i,1]
      my.fisher[1,2] = chi.input[i,2]
      my.fisher[2,1] = sum(chi.input[-i,1])
      my.fisher[2,2] = sum(chi.input[-i,2])
      
      fisher.output[i] = fisher.test(my.fisher)$p.value
    }
  }
  fisher.output = floor(fisher.output * 100000) / 100000
  fish.out = data.frame(chi.input, fisher.output)
  colnames(fish.out) = c("Exp","Obs","FET")
  return(fish.out)
}


##calculate the ChiSq distr similarity score.
calculate.chi <- function(processed.input, my.label=NULL)
{
  sum.cells = apply(processed.input[,3:6],1,max)
  inp = processed.input[sum.cells > 20,]
  processed.input = processed.input[sum.cells > 20,]
  
  ##use the calculated labels
  new.lab = rep("",nrow(processed.input))
  for(i in 1:nrow(processed.input))
  {
    new.lab[i] = paste(
      ifelse(processed.input[i,]$ery.lab,"Ery",""),
      ifelse(processed.input[i,]$ly.lab,"Ly",""),
      ifelse(processed.input[i,]$mk.lab,"Mk",""),
      ifelse(processed.input[i,]$my.lab,"My",""),
      sep="")
  }
  new.lab = as.factor(new.lab)
  summary(new.lab)
  
  ##calculate all pairwise chisq values.
  my.chi = matrix(rep(0, nrow(inp)*nrow(inp)), nrow = nrow(inp))
  for(i in 1:nrow(inp))
  {
    for(j in 1:nrow(inp))
    {
      inp.x = floor(inp[i,3:6] / (sum(inp[i,3:6])/100)+0.5)
      inp.y = floor(inp[j,3:6] / (sum(inp[j,3:6])/100)+0.5)
      inp.x[inp.x == 0] = inp.x[inp.x == 0] + 1
      inp.y[inp.y == 0] = inp.y[inp.y == 0] + 1
      max.x = which(inp.x  == max(inp.x))
      max.y = which(inp.y  == max(inp.y))
      
      inp.x[max.x] = 100 - sum(inp.x[-max.x])
      inp.y[max.y] = 100 - sum(inp.y[-max.y])
      
      chi.inp = rbind(inp.x, inp.y)
      my.chi[i,j] = chisq.test(t(chi.inp))$p.value
      if(my.chi[i,j] < 0.001){my.chi[i,j]  = 0.001}
      my.chi[i,j] = floor(my.chi[i,j] * 1000) / 1000
    }
  }
  
  my.chi.melt = data.frame(0,0,0,"","")
  colnames(my.chi.melt) = c("i","j","chi","lab.i","lab.j")
  for(i in 1:nrow(my.chi))
  {
    for(j in 1:nrow(my.chi))
    {
      to.add = data.frame(i,j,my.chi[i,j],new.lab[i], new.lab[j])
      colnames(to.add) = c("i","j","chi","lab.i","lab.j")
      my.chi.melt = rbind(my.chi.melt, to.add)
    }
  }
  my.chi.melt = my.chi.melt[2:nrow(my.chi.melt),]
  my.chi.melt = data.frame(my.chi.melt)
        
  ##create boxplots for distribution of ChiSq within a cell type and between cell types
  pdf(my.label, width = 5, height = 3)
  cell.labels = as.factor(new.lab)
  for(cell.lab in levels(as.factor(new.lab)))
  {
    chi.selected = my.chi.melt[my.chi.melt$lab.i == cell.lab,]
    chi.boxplot <- ggplot(chi.selected, aes(lab.j, chi, color = lab.j)) + 
      geom_boxplot() + coord_flip() + ggtitle(cell.lab)
    plot(chi.boxplot)
    # boxplot(chi.selected$chi ~ chi.selected$lab.j, main = cell.lab,
            # xlab="Cells", ylab="ChiSeq p value")
  }
  dev.off()
  return(my.chi)
}

calculate.fisher <- function(processed.input, my.label)
{
  sum.cells = apply(processed.input[,3:6],1,max)
  inp = processed.input[sum.cells > 20,]
  processed.input = processed.input[sum.cells > 20,]

  ##use the calculated labels
  new.lab = rep("",nrow(processed.input))
  for(i in 1:nrow(processed.input))
  {
    new.lab[i] = paste(
      ifelse(processed.input[i,]$ery.lab,"Ery",""),
      ifelse(processed.input[i,]$ly.lab,"Ly",""),
      ifelse(processed.input[i,]$mk.lab,"Mk",""),
      ifelse(processed.input[i,]$my.lab,"My",""),
      sep="")
  }
  new.lab = as.factor(new.lab)
  summary(new.lab)
  
  my.fisher.1 = matrix(rep(0, nrow(inp)*nrow(inp)), nrow = nrow(inp)) ##mk
  my.fisher.2 = matrix(rep(0, nrow(inp)*nrow(inp)), nrow = nrow(inp)) ##ery
  my.fisher.3 = matrix(rep(0, nrow(inp)*nrow(inp)), nrow = nrow(inp)) ##my
  my.fisher.4 = matrix(rep(0, nrow(inp)*nrow(inp)), nrow = nrow(inp)) ##ly
  
  inp[,3] = as.numeric(as.character(inp[,3]))
  inp[,4] = as.numeric(as.character(inp[,4]))
  inp[,5] = as.numeric(as.character(inp[,5]))
  inp[,6] = as.numeric(as.character(inp[,6]))
  
  for(i in 1:nrow(inp))
  {
    for(j in 1:nrow(inp))
    {
      inp.x = floor(inp[i,3:6] / (sum(inp[i,3:6]+1)/100)+0.5)
      inp.y = floor(inp[j,3:6] / (sum(inp[j,3:6]+1)/100)+0.5)
      inp.x[inp.x == 0] = inp.x[inp.x == 0] + 1
      inp.y[inp.y == 0] = inp.y[inp.y == 0] + 1
      max.x = which(inp.x  == max(inp.x))
      max.y = which(inp.y  == max(inp.y))
      
      inp.x[max.x] = 100 - sum(inp.x[-max.x])
      inp.y[max.y] = 100 - sum(inp.y[-max.y])
      
      fisher.inp = matrix(rep(0,4), nrow=2)
      for(k in 1:4)
      {
        fisher.inp[1,1] = as.numeric(as.character(inp.x[k])); 
        fisher.inp[2,1] = as.numeric(as.character(sum(inp.x[-k])))
        fisher.inp[1,2] = as.numeric(as.character(inp.y[k])); 
        fisher.inp[2,2] = as.numeric(as.character(sum(inp.y[-k])))
        if(k == 1){my.fisher.1[i,j] = fisher.test(fisher.inp)$p.value}
        if(k == 2){my.fisher.2[i,j] = fisher.test(fisher.inp)$p.value}
        if(k == 3){my.fisher.3[i,j] = fisher.test(fisher.inp)$p.value}
        if(k == 4){my.fisher.4[i,j] = fisher.test(fisher.inp)$p.value}
      }
      
    }
  }
  
  ##organise all info into one data frame
  my.fisher.melt = data.frame(0,0,0,"","","")
  colnames(my.fisher.melt) = c("i","j","chi","lab.i","lab.j","Type")
  for(i in 1:nrow(my.fisher.1))
  {
    for(j in 1:nrow(my.fisher.1))
    {
      if(processed.input[i,]$mk.lab + processed.input[j,]$mk.lab == 2)## if the mk was there in the first place
      {
      to.add = data.frame(i,j,my.fisher.1[i,j],new.lab[i], new.lab[j],"mk")
      colnames(to.add) = c("i","j","chi","lab.i","lab.j","Type")
      my.fisher.melt = rbind(my.fisher.melt, to.add)
      }
    }
  }
  
  for(i in 1:nrow(my.fisher.2))
  {
    for(j in 1:nrow(my.fisher.2))
    {
      if(processed.input[i,]$ery.lab + processed.input[j,]$ery.lab == 2)## if the ery was there in the first place
      {
      to.add = data.frame(i,j,my.fisher.2[i,j],new.lab[i], new.lab[j],"ery")
      colnames(to.add) = c("i","j","chi","lab.i","lab.j","Type")
      my.fisher.melt = rbind(my.fisher.melt, to.add)
      }
    }
  }
  
  for(i in 1:nrow(my.fisher.3))
  {
    for(j in 1:nrow(my.fisher.3))
    {
      if(processed.input[i,]$my.lab + processed.input[j,]$my.lab == 2)## if the my was there in the first place
      {
      to.add = data.frame(i,j,my.fisher.3[i,j],new.lab[i], new.lab[j],"my")
      colnames(to.add) = c("i","j","chi","lab.i","lab.j","Type")
      my.fisher.melt = rbind(my.fisher.melt, to.add)
      }
    }
  }
  
  for(i in 1:nrow(my.fisher.4))
  {
    for(j in 1:nrow(my.fisher.4))
    {
      if(processed.input[i,]$ly.lab + processed.input[j,]$ly.lab == 2)## if the ly was there in the first place
      {
      to.add = data.frame(i,j,my.fisher.4[i,j],new.lab[i], new.lab[j],"ly")
      colnames(to.add) = c("i","j","chi","lab.i","lab.j","Type")
      my.fisher.melt = rbind(my.fisher.melt, to.add)
      }
    }
  }
  
  my.fisher.melt = my.fisher.melt[2:nrow(my.fisher.melt),]
  my.fisher.melt = data.frame(my.fisher.melt)
  colnames(my.fisher.melt) = c("i","j","Fisher","lab.i","lab.j","Type")
  
  pdf(my.label, width =15, height = 3)
  cell.labels = as.factor(new.lab)
  for(cell.lab in levels(as.factor(new.lab)))
  {
    fisher.selected = my.fisher.melt[my.fisher.melt$lab.i == cell.lab,]
    fisher.boxplot <- ggplot(fisher.selected, aes(lab.j, Fisher, color = lab.j)) + 
      geom_boxplot() + coord_flip() + ggtitle(cell.lab)+xlab("Fisher exact test")+
      facet_grid(cols = vars(Type))
    plot(fisher.boxplot)
    # boxplot(chi.selected$chi ~ chi.selected$lab.j, main = cell.lab,
    # xlab="Cells", ylab="ChiSeq p value")
  }
  dev.off()
  
  my.return.list = list(my.fisher.1, my.fisher.2, my.fisher.3, my.fisher.4)
  return(my.return.list)
}

